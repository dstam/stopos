
/*
   Copyright 2013 Willem Vermin, SURFsara

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
#include "stopos_pool.h"
#include "files_pool.h"
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include "wutils.h"
#include <string.h>
#include <strings.h>
#include <stdio.h>
#include <algorithm>
#include <fstream>
#include "stopos.h"
#include <string>
#include <list>
#include <unistd.h>
#include <errno.h>

// constructor
files_pool::files_pool()
{
  this->whoami = "files";
}

// own version of set_db_name.
void files_pool::set_db_name(std::string filename)
{
  this->sdb_name = filename;
  this->db_name  = strdup(sdb_name.c_str());
}

void files_pool::create_fname(std::string &fname,const std::string &k)
{
  fname = this->sdb_name + "/" + k;
}

int files_pool::get_record(std::string &r, const std::string &k, const std::string &l)
{
  std::string fname;
  this->create_fname(fname,k);
  std::ifstream f;
  f.open(fname.c_str(),std::fstream::in);
  if (f.fail())
    return FETCHERROR;

  char b[this->reclen];

  f.getline(b,this->reclen);
  if (f.bad())
    return FETCHERROR;

  r = b;

  f.close();

  return 0;
}

int files_pool::put_record(const std::string &b, 
                           const std::string &k, 
			   const std::string &l)
{
  bool recordtoolong = 0;

  std::string buf = b;

  if (buf.size() >= reclen)
    recordtoolong=1;

  std::string fname;

  this->create_fname(fname,k);

  std::ofstream f;
  f.open(fname.c_str(),std::fstream::out);
  if (f.fail())
    return STOREERROR;

  buf.resize(reclen);

  f.write(buf.c_str(),this->reclen);
  if (f.bad())
    return STOREERROR;

  f.close();

  if(recordtoolong)
    return RECORDTOOLONG;

  return 0;
}

int files_pool::remove_record(const std::string &k,const std::string &l)
{
  int rc;
  std::string fname;
  this->create_fname(fname,k);
  rc = unlink(fname.c_str());
  if (rc == -1)
    return this->REMOVEERROR;

  return 0;
}

int files_pool::create_db(const std::string &dbname)
{
  int rc;
  rc = this->purge_db();
  if (rc != 0)
    return rc;

  rc = mkdir_p(dbname,0755);
  if (rc != 0)
    return rc;

  return 0;
}

int files_pool::purge_db(void)
{
  int rc;
  if (this->db_name == 0)
    return NOFILENAME;

  std::list <std::string> v;

  rc = get_dir_list(v,this->sdb_name);
  for (std::list<std::string>::iterator it=v.begin(); it != v.end(); ++it)
  {
    // . and .. will be automaticalle skipped by unlink
    std::string f = this->sdb_name + '/' + (*it);
    unlink(f.c_str());
  }

  rc = rmdir(this->sdb_name.c_str());
  if (rc != 0)
    if (errno == ENOENT)
      rc = 0;

  return rc;

#if 0
  std::string command;
  command = "rm -rf " + this->sdb_name;
  rc = system(command.c_str());
  if (rc != 0)
    ;
  return 0;
#endif
}

int files_pool::open_db(void)
{
  if (this->db_name == 0)
    return NOFILENAME;

  if (this->db_open)
    return 0;

  // create lockfilename
  this->lockfilename = this->sdb_name+"/0lockfile";

  int rc = access(this->db_name,R_OK|W_OK);
  if (rc == -1)
    return OPENERROR;

  struct stat status;
  stat(this->db_name, &status );

  if ( !(status.st_mode & S_IFDIR) )
     return OPENERROR;

  this->lockfile = open(this->lockfilename.c_str(),O_CREAT|O_RDWR,0600);
  if (this->lockfile < 0)
    return LOCKERROR;

  struct flock lock;
  lock.l_type = F_RDLCK|F_WRLCK;
  lock.l_whence = SEEK_SET;
  lock.l_start  = 0;
  lock.l_len    = 0;

  rc = fcntl(this->lockfile,F_SETLKW,&lock);
  if (rc < 0)
    return LOCKERROR;

  this->db_open = 1;

  return 0;
}

int files_pool::close_db(void)
{
  if (!this->db_open)
    return 0;

  struct flock lock;
  lock.l_type = F_UNLCK;
  lock.l_whence = SEEK_SET;
  lock.l_start  = 0;
  lock.l_len    = 0;

  int rc = fcntl(this->lockfile,F_SETLKW,&lock);
  if (rc < 0)
    return UNLOCKERROR;

  close(this->lockfile);

  this->db_open = 0;

  return 0;
}


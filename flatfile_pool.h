
/*
   Copyright 2013 Willem Vermin, SURFsara

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
#ifndef FLATFILE_POOL_H
#define FLATFILE_POOL_H
#include "stopos_pool.h"
#include "stopos_key.h"
#include "stopos.h"

class flatstatusrecord: statusrecord
{
  longuint nextd;
};

class flatfile_pool: public stopos_pool
{
  private:
    int ff;    // handle of file to be used

    int get_record(std::string &r, const std::string &k, const std::string &l);
    int put_record(const std::string &r, const std::string &k,const std::string &l);
    int remove_record(const std::string &k, const std::string &l);
    int write_string(const std::string &r,off_t p);
    int read_string(std::string &r,off_t p);

    const static char deleted = 'd';
    const static char exists  = '_';

    std::string key_to_slot(const std::string &k);
    off_t strtoslot(const std::string &s);
    std::string slottostr(off_t l);
    int haskey(std::string &r, const std::string &k);
    int open_db(int flags);

  public:
    flatfile_pool();
    int create_db(const std::string &dbname);
    int purge_db(void);
    int open_db(void);
    int close_db(void);
};

#endif

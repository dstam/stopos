
/*
   Copyright 2013 Willem Vermin, SURFsara

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
#ifndef STOPOS_KEY_H
#define STOPOS_KEY_H
#include <string>
#include <iostream>
#include "stopos.h"

class stopos_key
{
  protected:
    std::string value;
    std::string slot;
    static const char firstchar = 'a';
    static const char lastchar  = 'z';
    static const unsigned int base = lastchar - firstchar + 1;
    template <typename T> void tostring1(std::string &v,const T n)
    {
      if (n == 0)
        return;
      unsigned int c = n + firstchar - base*(n/base);
      tostring1(v,n/base);
      v.push_back(c);
    }
    template <typename T> void tostring(std::string &v,const T n)
    {
      if (n == 0)
	v = firstchar;
      else
	tostring1(v,n);
    }
  public:
    stopos_key();
    stopos_key(const std::string &s);
    void increment(void);
    std::string str(void) const;
    std::string get_key(void) const;
    std::string get_slot(void) const;
    void set_key(const std::string &k){ this->value = k;}
    void set_slot(const std::string &s){ this->slot = s;}
    void impossible();
    bool operator==(const stopos_key &k) const;
    bool operator!=(const stopos_key &k) const;

    // to assign an unsigned integer value to a key
    template <typename T> void set(const T k,const std::string &slot)
      {
        this->value = "";
        this->tostring(this->value,k);
	this->slot = slot;
        return;
      }

    // to get the unsigned integer value from a key
    template <typename T> T get() const
    {
      T r = 0;

      for (unsigned int i=0; i<this->value.size(); i++)
        r = this->base*r + this->value[i]-this->firstchar;

      return r;
    }

    static longuint KeyToNumber(const std::string s)
    {
      longuint r = 0;
      stopos_key d;
      for (unsigned int i=0; i<s.size(); i++)
        r = base*r + s[i]-firstchar;

      return r;
    }

    template <typename T> static std::string NumberToKey(const T n)
    {
      std::string s;
      stopos_key k;
      k.tostring(s,n);
      return s;
    }
};
#endif

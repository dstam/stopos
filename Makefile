
###
#   Copyright 2013 Willem Vermin, SURFsara
#
#   Licensed under the Apache License, Version 2.0 (the "License");
#   you may not use this file except in compliance with the License.
#   You may obtain a copy of the License at
#
#       http://www.apache.org/licenses/LICENSE-2.0
#
#   Unless required by applicable law or agreed to in writing, software
#   distributed under the License is distributed on an "AS IS" BASIS,
#   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
#   See the License for the specific language governing permissions and
#   limitations under the License.
###
include Make.inc
CGI_PROGRAMS       = stoposserver
CLIENT_PROGRAMS    = stoposclient stoposzztostr
CLIENT_PROGRAMS1   = stoposdump
OTHER_PROGRAMS     = testpool 

STOPOSSERVER_OBJS  := stoposserver.o stopos_pool.o stopos_key.o
STOPOSSERVER_LIBS  := libwutils.a

TESTPOOL_OBJS      := test_pool.o stopos_pool.o stopos_key.o clocks.o
TESTPOOL_LIBS      := libwutils.a

ifdef MAKE_GDBM
  COMPILE_FLAGS     += -DMAKE_GDBM
  STOPOSSERVER_OBJS +=  gdbm_pool.o
  STOPOSSERVER_LIBS += -lgdbm
  TESTPOOL_OBJS     +=  gdbm_pool.o
  TESTPOOL_LIBS     += -lgdbm
endif
ifdef MAKE_FLAT
  COMPILE_FLAGS     += -DMAKE_FLAT
  STOPOSSERVER_OBJS +=  flatfile_pool.o
  TESTPOOL_OBJS     +=  flatfile_pool.o
endif
ifdef MAKE_FILES
  COMPILE_FLAGS     += -DMAKE_FILES
  STOPOSSERVER_OBJS +=  files_pool.o
  TESTPOOL_OBJS     +=  files_pool.o
endif
ifdef MAKE_MYSQL
  COMPILE_FLAGS     += -DMAKE_MYSQL
  STOPOSSERVER_OBJS +=  mysql_pool.o
  STOPOSSERVER_LIBS += -lmysqlcppconn
  TESTPOOL_OBJS     +=  mysql_pool.o
  TESTPOOL_LIBS     += -lmysqlcppconn
endif

COMPILE_FLAGS += -D$(DB_DEFAULT)

COMPILE_FLAGS += -DSERVER_URL=\"$(SERVER_URL)\"

COMPILE_FLAGS += -DDB_DIR=\"$(DB_DIR)\"

STOPOSCLIENT_OBJS  = stoposclient.o wrequest.o shellenv.o
STOPOSCLIENT_LIBS  = -lcurl libwutils.a


STOPOSZZTOSTR_OBJS = stoposzztostr.o
STOPOSZZTOSTR_LIBS = libwutils.a -lz

PROGRAMS           = $(CGI_PROGRAMS) $(CLIENT_PROGRAMS) $(OTHER_PROGRAMS)

CGI_LIBS           = 
CGI_INC            = 

all: $(PROGRAMS)

CGI_OBJS           = $(STOPOSSERVER_OBJS)
CGI_SRCS           = $(CGI_OBJS:.o=.cpp)

CLIENT_OBJS        = $(STOPOSCLIENT_OBJS)
CLIENT_SRCS        = $(CLIENT_OBJS:.o=.cpp)

OTHER_OBJS         = $(TESTPOOL_OBJS)
OTHER_SRCS         = $(OTHER_OBJS:.o=.cpp)

SRCS = $(sort $(CGI_SRCS) $(OTHER_SRCS) $(CLIENT_SRCS) wutils.cpp)

stoposserver: $(STOPOSSERVER_OBJS) libwutils.a
	$(CXX) -o $@ $(CXXFLAGS) $(STOPOSSERVER_OBJS) $(STOPOSSERVER_LIBS)

stoposclient: $(STOPOSCLIENT_OBJS) libwutils.a
	$(CXX) -o $@ $(CXXFLAGS) $(STOPOSCLIENT_OBJS) $(STOPOSCLIENT_LIBS)

testpool:  $(TESTPOOL_OBJS) libwutils.a
	$(CXX) -o $@ $(CXXFLAGS) $(TESTPOOL_OBJS) $(TESTPOOL_LIBS)

stoposzztostr: $(STOPOSZZTOSTR_OBJS) libwutils.a
	$(CXX) -o $@ $(CXXFLAGS) $(STOPOSZZTOSTR_OBJS) $(STOPOSZZTOSTR_LIBS)

libwutils.a: wutils.o
	ar -r $@ wutils.o

stopos.1: stopos.man
	sed 's"DEFAULTSERVERURL"$(SERVER_URL)"' stopos.man > $@

%.o: 	%.cpp
	$(CXX) $(CXXFLAGS) $(COMPILE_FLAGS) $(CGI_INC) -c -o $@ $<

include .depend

.depend dep:
	$(CXX) -M $(SRCS) $(CGI_INC) > .depend

clean:
	rm -f *.o libwutils.a $(PROGRAMS) stopos.1 .depend

realclean: clean

install: install_cgi install_client install_module install_remote
	
install_cgi:	$(CGI_PROGRAMS)
	mkdir -p $(CGI_INSTALL_DIR)
	install $(CGI_PROGRAMS) $(CGI_INSTALL_DIR)

install_client: $(CLIENT_PROGRAMS) $(CLIENT_PROGRAMS1) stopos.1
	mkdir -p $(CLIENT_INSTALL_DIR)/bin
	install $(CLIENT_PROGRAMS) $(CLIENT_PROGRAMS1) $(CLIENT_INSTALL_DIR)/bin
	mkdir -p $(CLIENT_INSTALL_DIR)/man/man1
	install stopos.1 -m 644 $(CLIENT_INSTALL_DIR)/man/man1
	install sara-get-mem-size $(CLIENT_INSTALL_DIR)/bin
	install sara-get-num-cores $(CLIENT_INSTALL_DIR)/bin
	install sara-get-mem-size.1 -m 644 $(CLIENT_INSTALL_DIR)/man/man1
	install sara-get-num-cores.1 -m 644 $(CLIENT_INSTALL_DIR)/man/man1

install_module:
	mkdir -p $(MODULE_INSTALL_DIR)/mystopos
	install modulefile -m 644 $(MODULE_INSTALL_DIR)/mystopos/$(STOPOS_VERSION)

install_remote: $(CLIENT_PROGRAMS) $(CGI_PROGRAMS)
	chmod 755 $(CLIENT_PROGRAMS) $(CGI_PROGRAMS)
	@echo "Now, manually transport"
	@echo "$(CLIENT_PROGRAMS) to $(REMOTE_CLIENT_INSTALL_DIR)"
	@echo "and"
	@echo "$(CGI_PROGRAMS) to $(REMOTE_CGI_INSTALL_DIR)"



/*
   Copyright 2013 Willem Vermin, SURFsara

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
#include <iostream>
#include <string>
#include "stopos.h"
#include "wrequest.h"
#include <stdlib.h>
#include <curl/curl.h>
#include <fstream>
#include <sys/stat.h>
#include <sys/types.h>
#include <errno.h>

//constructor
wrequest::wrequest(void)
{
  this->set_configfilename();
  curl_global_init(CURL_GLOBAL_ALL);
  this->curl = curl_easy_init();
  this->debugging = 0;
}

//destructor
wrequest::~wrequest()
{
  curl_easy_cleanup(curl);
  curl_global_cleanup();
}

void wrequest::set_configfilename(void)
{
  char *h = getenv("HOME");
  std::string home;
  if (h == 0)
    home = "/";
  else
    home = h;
  this->configdirname = home + "/.stopos";
  this->configfilename = this->configdirname + "/id";
}

int wrequest::read_config(void)
{
  int rc = get_file_contents(this->config,this->configfilename);
  if (rc == 0)
  {
    size_t p= this->config.find_first_of('\n');
    if (p != this->config.npos)
      this->config.resize(p);
    return 0;
  }
  else
    return 1;
}

int wrequest::write_config(void)
{
  std::ofstream f;
  int rc;

  rc = mkdir(this->configdirname.c_str(),0700);
  if (rc < 0)
    if (errno != EEXIST)
      return 1;
  f.open(this->configfilename.c_str(),std::ios::out);
  if (f.fail())
    return 1;
  this->config = gen_random(this->config_size)+'\n';
  f << this->config;
  f.close();
  rc  = chmod(this->configfilename.c_str(),0600);
  if (rc < 0)
    return 1;
  return 0;
}


size_t write_data(char *p, size_t size, size_t nmemb, std::string *userdata)
{
  size_t l = size*nmemb;
  for (size_t i=0; i<l; i++)
    (*userdata).push_back(p[i]);
  return l;
}

int wrequest::set_serverurl(const std::string &u)
{
  this->serverurl = u + '?';
  curl_easy_setopt(this->curl,CURLOPT_HEADER,0L);
  curl_easy_setopt(this->curl,CURLOPT_NOPROGRESS, 1L);
  curl_easy_setopt(this->curl,CURLOPT_WRITEFUNCTION, write_data);

  return 0;
}

void wrequest::compose_message(std::string &m)
{
  m="";
  m += zstrtohex("stopos")                                    + httpsep;
  m += zstrtohex(this->message.get_protocol()               ) + httpsep;
  m += zstrtohex(this->message.get_whoami()+"."+this->config) + httpsep;
  m += zstrtohex(this->message.get_pool()                   ) + httpsep;
  m += zstrtohex(this->message.get_command()                ) + httpsep;
  m += zstrtohex(this->message.get_multi()                  ) + httpsep;
  m += zstrtohex(this->message.get_value()                  );
}

int wrequest::send_message(std::string &body, std::string &header)
{

  body.clear();
  header.clear();
  std::string m;
  this->compose_message(m);
  std::string x;
  x = this->serverurl+m;
  curl_easy_setopt(this->curl,CURLOPT_URL,x.c_str());
  curl_easy_setopt(this->curl,CURLOPT_WRITEDATA, &body);
  curl_easy_setopt(this->curl,CURLOPT_WRITEHEADER, &header);
  int rc;
  rc = curl_easy_perform(this->curl);

  return rc;
}


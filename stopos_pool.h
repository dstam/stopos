
/*
   Copyright 2013 Willem Vermin, SURFsara

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
//
// stopos_pool is the base class responsible for the logic
// behind adding, removing etc lines in the database.
//
// The access to the database itself is the task for the derived
// class.
//
//  In the following: a key is unique, never will two records
//  have the same slot in the same databse between two calls
//  of create_db or betwen create_db and purge_db.
//
// The derived class must provide the following functions:
//
//  std::string key_to_slot(const std::string &k)
//    given a key k, the function should return
//    a slot, where a record with key k can be
//    stored. If the database does not need a
//    slot (mysql, gdbm), then return "".
//    This slot will be used in subsequent put_record
//    and get_record calls.
//
//  int get_record(std::string &r,
//                         const std::string &k, 
//                         const std::string &l)
//    This function must, given the key k, and the slot l,
//      return the record in r. Return value = 0 if 
//      everything ok.
//
//
//   int put_record(const std::string &r, 
//	            const std::string &k, 
//		    const std::string &l)
//     This function shoud store record r, given key k
//     and slot l. The slot may be ignored.
//
//   int remove_record(const std::string &k, 
//	               const std::string &l)
//     This function should remove the record given key k
//       and slot l.
//
//
//   int create_db(const std::string &dbname)
//     This function should create the database. dbname is
//     a string containing the filename, tablename or
//     whatever. The caller (stoposserver) should know about the
//     issues involved.
//
//  int purge_db(void)
//    This function has to remove the database, the name
//    is in this->db_name  (char *) and
//          this->sdb_name  (std::string)
//
//  int open_db(void)
//    This function has to open the database, such that 
//    subsequent put_record, get_record, remove_record
//    calls can function.
//    See purge() above for the name of the database.
//
//  int close_db(void)
//    This function is called when database operations
//    are done, normally it should close the database.
//    See purge() above for the name of the database.
//
// Derived classes have no knowledge of the structure of
// the records, and can add extra information if so
// desired, as long as a retrieved record is equal
// to a stored record with the same key/slot combination.
//
// Also the "status record" is for usage of the base class only,
// with one exception: the field "extra" i.e. this->status->extra
// is not used by the base class and can be used by derived classes
// at will.
//

#ifndef STOPOS_POOL_H
#define STOPOS_POOL_H
#include <string>
#include "stopos.h"
#include "stopos_key.h"
#include <iostream>
 
class datarecord
{
  public:
    datarecord();
    datarecord(const std::string &s,char sep);
    stopos_key prev;
    stopos_key next;
    longuint retrieved;
    std::string value;
    std::string str(char dbsep) const;
};

class statusrecord
{
  public:
  stopos_key next;      // points to first line for the 'next' command
  stopos_key first;     // points to first record
  stopos_key last;      // points to last record
  stopos_key nextd;     // points to record to be dumped
  longuint count;       // total number of records added
  longuint present;     // total number of records now availabe
  longuint present0;    // ditto, but uncommitted
  std::string extra;    // string that can be used by derived class

  statusrecord(void);
  statusrecord(const std::string &s,char dbsep);
  std::string str(char dbsep);
  void init(void);
};

class stopos_pool
{
  protected:
    char* db_name;        // name for database
    std::string sdb_name; // idem
    static const unsigned int reclen = 4096;
    bool kvp; //true if Koot-Vermin paradigm is to be used
    bool db_open;
    
    stopos_key statuskey;
    stopos_key nextkey;
    statusrecord status;

    virtual std::string key_to_slot(const std::string &k) = 0;
    virtual int create_db(const std::string &dbname)      = 0;
    virtual int get_record(std::string &r,       
	                   const std::string &k, 
			   const std::string &l)          = 0;
    virtual int put_record(const std::string &r, 
	                   const std::string &k, 
			   const std::string &l)          = 0;
    virtual int remove_record(const std::string &k, 
	                      const std::string &l)       = 0;

    int put_status(void);
    int get_status(void);

    // the function add_line calls this to get a slot
    // from the derived class.  If no slot is necessary,
    // this function may return "".
    // The subsequent put_record call will have the return
    // value of this function as parameter.

    int get_record(datarecord &r,const stopos_key &k);
    int put_record(const datarecord &r,const stopos_key &k);
    int remove_record(const stopos_key &k);

    std::string whoami;

  public:

    stopos_pool();
    ~stopos_pool();

    virtual int purge_db(void)  = 0;
    virtual int open_db(void)   = 0;
    virtual int close_db(void)  = 0;


    int create_db(void);

    virtual void set_db_name(std::string filename);
    virtual std::string get_db_name(void);

    virtual int add_line(const std::string line,std::string &key);
    virtual int get_line(std::string &line,longuint &retrieved,std::string &key);
    virtual int remove_line(const std::string &key);

    virtual void set_kvp(const bool v);
    virtual bool get_kvp(void);

    int get_counts(longuint &count, 
		   longuint &present, 
		   longuint &present0);

    int dump_line(std::string  &line,
	          longuint     &retrieved,
		  std::string  &key);

    bool is_statusrecord(const std::string &k);

    const static int ERROR             =  1; 
    const static int NOFILENAME        =  2;
    const static int OPENERROR         =  3;
    const static int STOREERROR        =  4;
    const static int FETCHERROR        =  5;
    const static int NOTFOUND          =  6;
    const static int NOTIMPLEMENTED    =  7;
    const static int REMOVEERROR       =  8;
    const static int LOCKERROR         =  9;
    const static int CLOSEERROR        = 10;
    const static int UNLOCKERROR       = 11;
    const static int DBNOTOPEN         = 12;
    const static int CREATEERROR       = 13;
    const static int PURGEERROR        = 14;
    const static int RECORDTOOLONG     = 15;

};
#endif

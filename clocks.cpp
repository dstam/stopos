
/*
   Copyright 2013 Willem Vermin, SURFsara

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
#include <sys/times.h>
#include <sys/time.h>
#include <time.h>
#include <stdlib.h>
#include "clocks.h"
#include <unistd.h>
double cputime() /* aix, xlf */
{ struct tms b;
  clock_t r;
  times( &b);
  r = b.tms_utime + b.tms_stime;
  return ( (double) r/(double) sysconf(_SC_CLK_TCK));
}
double CPUTIME() /* cray  */
{ 
  return ( cputime());
}
double cputime_() /* g77, gcc */
{ 
  return ( cputime());
}

double wallclock() 
{ 
	struct timeval toot;
	double r;

	gettimeofday(&toot,0);
	r=toot.tv_sec+0.000001*(double)toot.tv_usec;
	return(r);
}
double WALLCLOCK()
{
  return (wallclock());
}
double wallclock_()
{
  return wallclock();
}
		
void fortransleep(int *i)
  {
    sleep(*i);
  }
 
void FORTRANSLEEP(int *i)
  {
    sleep(*i);
  }

void fortransleep_(int *i)
  {
    sleep(*i);
  }


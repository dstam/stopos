
/*
   Copyright 2013 Willem Vermin, SURFsara

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
#include <string>
#include <vector>
// 
// splits string s to vector v, using separator character sep
// return value: v
// It seems that it follows the python implementation of split()
//
std::vector <std::string> split(std::vector <std::string> &v, const std::string s, const char sep)
{
  v.clear();
  for (size_t p=0, q=0; q != s.npos; p = q + 1)
    v.push_back(s.substr(p,(q=s.find(sep,p))-p));
  return v;
}
#if 0
#include <iostream>
int main()
{
  char sep = '/';
  std::string s="/version/command/ /modifier/";
  std::vector <std::string> v;

  v=split(v,s,sep);
  for (unsigned i=0; i<v.size(); i++)
    std::cout << i << ":" << v[i] << ":" << std::endl;
  return 0;
}
#endif


/*
   Copyright 2013 Willem Vermin, SURFsara

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
#include <string>
#include <vector>
#include "stopos.h"
#include "wutils.h"
#include "stopos_key.h"

// constructor
stopos_key::stopos_key()
{
  this->set(0,"");
}

stopos_key::stopos_key(const std::string & s)
{
  std::vector <std::string> v;
  v = split(v,s,keysep);
  this->value = v[0];
  if (v.size()>1)
    this->slot = v[1];
}

void stopos_key::impossible()
{
  this->value="%%%%";
}

void stopos_key::increment(void)
{ // notused
  int p = this->value.size();
  while(1)
  {
    p--;
    if (this->value[p] == lastchar)
    {
      this->value[p] = firstchar;
      if (p == 0)
      {
	this->value = firstchar + this->value;
	p = 1;
      }
    }
    else
    {
      this->value[p] ++;
      break;
    }
  }
}

std::string stopos_key::str(void) const
{
  if (this->slot == "")
    return value;

  return this->value+keysep+this->slot;
}

std::string stopos_key::get_key(void) const
{
  return this->value;
}

std::string stopos_key::get_slot(void) const
{
  return this->slot;
}

bool stopos_key::operator==(const stopos_key &k) const
{
  return k.value == this->value;
}

bool stopos_key::operator!=(const stopos_key &k) const
{
  return k.value != this->value;
}


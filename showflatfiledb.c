
/*
   Copyright 2013 Willem Vermin, SURFsara

   Licensed under the Apache License, Version 2.0 (the "License");
   you may not use this file except in compliance with the License.
   You may obtain a copy of the License at

       http://www.apache.org/licenses/LICENSE-2.0

   Unless required by applicable law or agreed to in writing, software
   distributed under the License is distributed on an "AS IS" BASIS,
   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
   See the License for the specific language governing permissions and
   limitations under the License.
 */
#include <stdio.h>
int main()
{
  int c;
  int s=1;
  while ((c=getchar())!=EOF)
  {
    if (c == 0)
    {
      if (s) putchar('\n');
      s=0;
    }
    else
    {
      putchar(c);
      s=1;
    }
  }
}
